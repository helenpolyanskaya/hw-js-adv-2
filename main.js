// наприклад, коли є опроснік, і користувач не в вірному форматі водить відровіді. Ловітся помилка і запитується ще раз( можно з підказкаю додатковою як треба)
// бухоблік, і десь не заповнений осередок, що модже призвести доо невірного підрахунку балансу. 


const books = [

    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 

    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 

    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 

    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 

    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },

    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
];


function getList(c, d){
    let countBooks = books.length;
    let div = document.getElementById("root");
    let ol = document.createElement("ol");
    div.append(ol);
    
    let fragment = new DocumentFragment();
    let mustProperties = ['author', 'name', 'price'];
    
    for (book of books){
        try{           
            for (property of mustProperties){
                    if ( book.hasOwnProperty(property) == false) {
                        throw "No " + property + " for book";
                    }       
            }

            let contentBook = ""
            for (key in book) {
                contentBook += key.charAt(0).toUpperCase()+key.slice(1)+ ': ' + book[key] + ', ';
            }
            let li = document.createElement("li");
            li.insertAdjacentHTML('afterbegin', contentBook)
            fragment.append(li); 
        }
        
        catch (e) {
            console.log("Got error for: " + e);
        }    
    }   
    return ol.append(fragment);
}

getList(books, document.body);   

